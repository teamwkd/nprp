$(document).ready(function (){
    $('.hero_slider').owlCarousel({
      loop: true,
      margin:20,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay:false,
      slideSpeed : 3000,
      autoplayTimeout:3000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
        }
      }
    });

  });

  
  $(document).ready(function (){
    $('.client__slider').owlCarousel({
      loop: true,
      margin:0,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay:true,
      autoplaySpeed: 5000,
      slideTransition: 'linear',
      autoplayTimeout: 5000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 5,
          nav: false
        },
        1000: {
          items: 5,
        }
      }
    });

  });
  
  $(document).ready(function () {
    $(window).scroll(function() {
      var sticky = $('.header_s'),
        scroll = $(window).scrollTop();
       
      if (scroll  >=40) { 
        sticky.addClass('fixed'); }
      else { 
       sticky.removeClass('fixed');
    
    }
    });
  })


  $(document).ready(function () {
     $('.mobo_button').click(function(){
      $(this).toggleClass('cross_bar')
      $('.mobo__menu_link').toggleClass('active_menu');    
     }); 
  });


  $('.counter').each(function() {
    var $this = $(this),
        countTo = $this.attr('data-count');
    
    $({ countNum: $this.text()}).animate({
      countNum: countTo
    },
  
    {
  
      duration: 8000,
      easing:'linear',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
        //alert('finished');
      }
  
    });  
    
    
  
  });

  $(document).ready(function (){
    $('.testimonial_slider').owlCarousel({
      loop: true,
      margin:20,
      nav:false,
      dotsEach: true,
      dotData: true,
      responsiveClass: true,
      autoplay: true,
      slideSpeed : 3000,
      autoplayTimeout:3000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
        }
      }
    });

  });

  $(document).ready(function (){
    $('.team_sec_slider').owlCarousel({
      loop: true,
      margin:0,
      nav: true,
      dotsEach: true,
      dotData: true,
      navText: ["<img src='assets/img/ar_lt.png'>", "<img src='assets/img/ar_rt.png'>"],
      responsiveClass: true,
      autoplay: true,
      slideSpeed : 3000,
      autoplayTimeout:3000,
      autoplayHoverPause:true,    
      owldots:true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 4,
        }
      }
    });

  });

  $('#myTab a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })




  (function(){
    let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    let isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
    let scrollbarDiv = document.querySelector('.scrollbar');
      
  });