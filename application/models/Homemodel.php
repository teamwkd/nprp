<?php
class Homemodel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
         $this->load->database();
        // $this->table_name = 'testimonial';
    }
    function get_user($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $query = $this->db->get();
       return$query->result_array(); 
     }

     function check_is_valid_user($module_name = '')
     {
         //echo $this->session->userdata('sess_admin_id');
         if($this->session->userdata('sess_user_email') == '')
         {
             redirect('home');
         }
     } 
   
    

     function get_Testimonials()
     {
            $this->db->select('*');
            $this->db->from('testimonial');
            $this->db->where('status','active');
            $this->db->order_by('id', 'DESC');
            $query = $this->db->get();
           return$query->result_array(); 
     }

     function get_About()
     {
        $this->db->select('*');
        $this->db->from('about');
        $this->db->where('status','active');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return$query->result_array(); 
     }

     function addContact($data){
       if(!empty($data)){
         unset($data['sbt']);
          $this->table_name='contact';
          $this->db->insert($this->table_name,$data);
           $insert_id = $this->db->insert_id();
           return $insert_id;
          //echo $this->db->last_query(); die;
       }
   }

   function get_Team()
   {
    $this->db->select('*');
    $this->db->from('team');
    $this->db->where('status','active');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
   return$query->result_array(); 
   }

   function get_Service()
   {
    
    $this->db->select('*');
    $this->db->from('service');
    $this->db->where('status','active');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
   return$query->result_array(); 

   }

   function get_Details($id)
   {
    $this->db->select('*');
    $this->db->from('service');
    $this->db->where('id',$id);
    $this->db->where('status','active');
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get();
   return$query->result_array(); 

   }

}   