<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()

   {
		   $this->load->helper(array('form','url','common'));
			$this->load->library(array('form_validation','session','cart'));
         $this->load->model('homemodel','',TRUE); 

         $data['member']=$this->homemodel->get_Team();
         $data['testimon']=$this->homemodel->get_Testimonials();
         $data['service']=$this->homemodel->get_Service();
         
         $this->load->view('frontend/assets/header',$data);
			$this->load->view('frontend/index',$data);
			$this->load->view('frontend/assets/footer',$data);
	}


	function service_details($id)
   
   {
      $this->load->helper(array('form', 'url','common'));
      $this->load->library(array('form_validation','session','cart'));
      $this->load->model('homemodel','',TRUE);
     
      if(isset($_GET['id'])) {
		$id = $_GET['id']; 
	   } 

      $data['about']=$this->homemodel->get_About();
      $data['member']=$this->homemodel->get_Team();
      $data['testimon']=$this->homemodel->get_Testimonials();
      $data['service']=$this->homemodel->get_Service();
      $data['record']=$this->homemodel->get_Details($id);

     
     $this->load->view('frontend/assets/header',$data);
	  $this->load->view('frontend/service-detail',$data);
     $this->load->view('frontend/assets/footer',$data);
     
   }

	public function about()
   
   {
		  $this->load->helper(array('form','url','common'));
        $this->load->library(array('form_validation','session'));
		  $this->load->model('homemodel','',TRUE); 

        $data['about']=$this->homemodel->get_About();
        $data['member']=$this->homemodel->get_Team();
        $data['testimon']=$this->homemodel->get_Testimonials();
        $data['service']=$this->homemodel->get_Service();

       
        $this->load->view('frontend/assets/header',$data);
		  $this->load->view('frontend/about-us',$data);
		  $this->load->view('frontend/assets/footer',$data);
	
	}


	public function contact()
   
   {
		$this->load->helper(array('form','url','common'));
      $this->load->library(array('form_validation','session'));
		$this->load->model('homemodel','',TRUE); 
     
      $data['member']=$this->homemodel->get_Team();
      $data['testimon']=$this->homemodel->get_Testimonials();
      $data['service']=$this->homemodel->get_Service();

     
      $this->load->view('frontend/assets/header',$data);
		$this->load->view('frontend/contact-us',$data);
		$this->load->view('frontend/assets/footer',$data);
   }	
   
   public function service()
  
   {
      $this->load->helper(array('form','url','common'));
      $this->load->library(array('form_validation','session'));
      $this->load->model('homemodel','',TRUE); 
      
      $data['member']=$this->homemodel->get_Team();
      $data['testimon']=$this->homemodel->get_Testimonials();
      $data['service']=$this->homemodel->get_Service();

	   $this->load->view('frontend/assets/header',$data);
		$this->load->view('frontend/service',$data);
		$this->load->view('frontend/assets/footer',$data);
   }

   public function team()
  
   {
      $this->load->helper(array('form','url','common'));
      $this->load->library(array('form_validation','session'));
      $this->load->model('homemodel','',TRUE); 
      
      $data['member']=$this->homemodel->get_Team();
      $data['testimon']=$this->homemodel->get_Testimonials();
      $data['service']=$this->homemodel->get_Service();

	   $this->load->view('frontend/assets/header',$data);
		$this->load->view('frontend/our-team',$data);
		$this->load->view('frontend/assets/footer',$data);
   }

	function addContact()
  
   {
      $this->load->helper(array('form', 'url'));
      $this->load->library('email');
      $this->load->library(array('session','pagination'));
      $this->load->model('homemodel','',TRUE);  
       
        if(isset($_POST['email'])){
            $email=$_POST['email'];
        } 
    
        if(isset($_POST['mobile'])){
            $mobile=$_POST['mobile'];      
          }
        if(isset($_POST['subject'])){
            $sub=$_POST['subject'];
        }
        if(isset($_POST['message'])){
            $content=$_POST['message'];
             
         }
    
      if(!empty($this->input->post())){ 
        if (isset($_POST['sbt'])) {
			$insert_id=$this->homemodel->addContact($this->input->post());
            $name =  $_POST['username'];
            $email = $_POST['email'];
			$phone = $_POST['mobile'];
			$sub =  $_POST['subject'];
            $content = $_POST['message'];
            $to = "ssing648@gmail.com";
        
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers = "From: himanshu@webkidukan.com" . "\r\n";
            $headers = 'MIME-Version: 1.0';
            $headers = 'Content-type: text/html; charset=iso-8859-1';
            
            $message = '<html>';
            $message .= '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            $message .='<link rel="stylesheet" href="https://aduclick.in/assets/css/custom.css">';
            $message .='</head>';
            $message .='<body style="background-color:#f6f6f6;  box-sizing: border-box; font-family: "Poppins", sans-serif;">';
            $message .='<div style="margin:0px auto;background: #f3f3f3;padding: 10px;">';
            $message .='<div style="border: 1px solid #ccc;background-color:#fff;clear:both;color:#666;line-height:32px;max-width:500px;margin:50px auto;border-radius:5px 5px 5px 5px">';
            $message .='<div style="color:#fff;text-align:center;padding:0px 0px 0px;background-repeat:no-repeat;background-position:center;background-size:183px;background-color:#1d6e9c;width:100%;height:125px;border-radius:5px 5px 0px 0px">';
            $message .='<img src="http://nightouts.in/vedangac.com/assets/img/pan_logo.png" style=" display: block; margin: 0 auto;width: 152px; padding-top: 15px;">';
            $message .='</div>';
            $message .='<div style="margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;padding-top:0;padding-right:0;padding-bottom:0px;padding-left:0;font-size:15px">';
            $message .='<img src="http://nightouts.in/vedangac.com/assets/img/message.png" style=" display: block; margin: 0 auto;width: 100px; padding-top: 15px;">';
            $message .='<table cellpadding="0" cellspacing="0" style="width: 100%;margin: 8px 1px;font-size: 12px;font-size:12px">';
            $message .='<tbody>';
            $message .='<tr>';
            $message .='<td style="border-bottom:1px solid #eeee;text-align:left;padding: 10px;color: #000;width: 171px;font-weight:bold;font-size: 15px;background: url(http://nightouts.in/vedangac.com/new-dev/dots.png);background-size: 3px;background-repeat: no-repeat;background-position: right;">Name</td>';
            $message .='<td style="border-bottom:1px solid #eeee;font-size: 17px;padding: 10px;color: #000;text-align: left; "> '.$name.' </td>';       
            $message .='</tr>';
            $message .='<tr>';
            $message .='<td style="border-bottom:1px solid #eeee;text-align:left;padding: 10px;color: #000;width: 171px;font-weight:bold;font-size: 15px;background: url(http://nightouts.in/vedangac.com/new-dev/dots.png);background-size: 3px;background-repeat: no-repeat;background-position: right;">Email</td>';
            $message .='<td style="border-bottom:1px solid #eeee;font-size: 17px;padding: 10px;color: #000;text-align: left; "> '.$email.' </td>';       
            $message .='</tr>';
            $message .='<tr>';
            $message .='<td style="border-bottom:1px solid #eeee;text-align:left;padding: 10px;color: #000;width: 171px;font-weight:bold;font-size: 15px;background: url(http://nightouts.in/vedangac.com/new-dev/dots.png);background-size: 3px;background-repeat: no-repeat;background-position: right;">Phone</td>';
            $message .='<td style="border-bottom:1px solid #eeee;font-size: 17px;padding: 10px;color: #000;text-align: left;">'.$phone.'</td>';
            $message .='</tr>';
            $message .='<tr>';
            $message .='<td style="border-bottom:1px solid #eeee;text-align:left;padding: 10px;color: #000;width: 171px;font-weight:bold;font-size: 15px;background: url(http://nightouts.in/vedangac.com/new-dev/dots.png);background-size: 3px;background-repeat: no-repeat;background-position: right;">Subject</td>';
            $message .='<td style="border-bottom:1px solid #eeee;font-size: 17px;padding: 10px;color: #000;text-align: left;">'.$sub.'</td>       ';      
            $message .='</tr>';
            $message .='<tr>';
            $message .='<td style="border-bottom:1px solid #eeee;text-align:left;padding: 10px;color: #000;width: 171px;font-weight:bold;font-size: 15px;background: url(http://nightouts.in/vedangac.com/new-dev/dots.png);background-size: 3px;background-repeat: no-repeat;background-position: right;">content</td>';
            $message .='<td style="border-bottom:1px solid #eeee;font-size: 17px;padding: 10px;color: #000;text-align: left;font-weight:bold;">'.$content.'</td>';
            $message .='</tr>';
            $message .='</tbody>';
            $message.='</table>';
            $message .='<p style="padding: 10px 30px;font-weight: 600;text-align: center;font-size: 14px;margin-bottom: 0;background: #000;color: #fff;text-transform: capitalize;"> © 2020 Vedang | All right reserved</p>';
            $message .='</div>';
            $message .='</div>';      
            $message .='</div>';
            $message .='</div>';
            $message .='</body>';
            $message .='</html>';
         
                       $t = mail($to, $name, $message, $headers);
                    // var_dump($t);exit;
           if ($t) {
              echo "<script type='text/javascript'>alert('Your email was sent successfully.');</script>";
           
           }else{
            echo "<script type='text/javascript'>alert('Your email has not been sent.');</script>";
               }
          
  }
		
        echo "<script>
        alert('Thank you for generating enquiry. We will get back to you soon.');
        window.location.href = '" . base_url() . "home';
        </script>";
      }

   
   }
  
}
