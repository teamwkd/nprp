<?php
class Contact extends CI_Controller {
   
   public function __construct() {
      parent::__construct ();
      $this->load->helper('download');
     }
    
   function index()
   {  
      $this->load->helper(array('form', 'url','common'));
      $this->load->library(array('session','pagination'));
      
      $this->load->model('loginmodel','',TRUE);     
      $this->loginmodel->check_is_valid_user();
       
      $this->load->model('contactmodel','',TRUE);     
      // $insert_id=$this->contactmodel->add($this->input->post());
       
      
      
      $num_records = $this->contactmodel->get_total_records();
     
     
      $data['num_records']  = $num_records;
      $base_url=base_url().'admin/contact/index';
      $start_index=$this->uri->segment($this->uri->total_segments());
      $default_page_size = 5;
      if( $this->input->get('per_page') != '' ){
         $per_page = $this->input->get('per_page');
      }
      else{
         $per_page=$default_page_size;
      }
      
      $data['default_page_size']  = $default_page_size;
      $content_array=$this->contactmodel->get_contents($per_page,$start_index);
      $data['content_array']=$content_array;
      $data['start_index']=$start_index;
      
      $data['pagination_links']=paging($base_url,$this->input->server("QUERY_STRING"),$num_records,$per_page,$this->uri->total_segments());
      $data['values']=$this->contactmodel->getAllAdmin();
      $this->load->view('admin/assets/header',$data);  
      $this->load->view('admin/assets/sidebar',$data);
      $this->load->view('admin/contact/index',$data);
      $this->load->view('admin/assets/footer',$data);  
   }
   
}