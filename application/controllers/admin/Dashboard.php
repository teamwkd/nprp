<?php
class Dashboard extends CI_Controller {
    
   function index()
   {
      $this->load->helper(array('form', 'url'));
      $this->load->library(array('session','pagination'));
      
      $this->load->model('loginmodel','',TRUE);     
      $this->loginmodel->check_is_valid_user();    
      
      
      //  $data = array(
      //    'header' => $this->load->view('admin/assets/header', '', TRUE),
      //    'sidebar' => $this->load->view('admin/assets/sidebar', '', TRUE),           
      //    'footer' => $this->load->view('admin/assets/footer', '', TRUE),
      // );
        $data['values']=$this->loginmodel->getAllAdmin();
        $this->load->view('admin/assets/header',$data);
        $this->load->view('admin/assets/sidebar',$data);
        $this->load->view('admin/dashboard',$data);
        $this->load->view('admin/assets/footer',$data);
   }

   public function adminData(){
    
      $this->load->helper(array('form', 'url'));
      $this->load->library(array('session','pagination'));
      
      $this->load->model('loginmodel','',TRUE);     
      $this->loginmodel->check_is_valid_user();    
      
     $data['values']=$this->loginmodel->getAllAdmin();

   }
   

}