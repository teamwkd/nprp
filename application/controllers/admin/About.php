<?php
class About extends CI_Controller {
   
   public function __construct() {
      parent::__construct ();
      $this->load->helper('download');
     }
    
   function index()
   {
      $this->load->helper(array('form', 'url','common'));
      $this->load->library(array('session','pagination'));
      
      $this->load->model('loginmodel','',TRUE);     
      $this->loginmodel->check_is_valid_user();    
       
      $this->load->model('aboutmodel','',TRUE);     
       
   
      $num_records = $this->aboutmodel->get_total_records();
     
     
      $data['num_records']  = $num_records;
      $base_url=base_url().'admin/about/index';
      $start_index=$this->uri->segment($this->uri->total_segments());
      $default_page_size = 5;
      if( $this->input->get('per_page') != '' ){
         $per_page = $this->input->get('per_page');
      }
      else{
         $per_page=$default_page_size;
      }
      
      $data['default_page_size']  = $default_page_size;
      $content_array=$this->aboutmodel->get_contents($per_page,$start_index);
      $data['content_array']=$content_array;
      $data['start_index']=$start_index;
      
      $data['pagination_links']=paging($base_url,$this->input->server("QUERY_STRING"),$num_records,$per_page,$this->uri->total_segments());
      $data['values']=$this->aboutmodel->getAllAdmin();
      $this->load->view('admin/assets/header',$data);  
      $this->load->view('admin/assets/sidebar',$data);
      $this->load->view('admin/about/index',$data);
      $this->load->view('admin/assets/footer',$data);
   }

   function add(){
       $this->load->helper(array('form', 'url'));
       $this->load->library(array('session','pagination'));
        $this->load->model('aboutmodel','',TRUE);     
        
       if(!empty($this->input->post())){
          $insert_id=$this->aboutmodel->add($this->input->post());
          self::upload_file($insert_id,$_FILES,'image1','add');
          self::upload_file($insert_id,$_FILES,'image2','add'); 
          self::upload_file($insert_id,$_FILES,'image3','add');

          $this->session->set_flashdata('message',"About has been added.");
          redirect("admin/about");  
       }
           
           $data['values']=$this->aboutmodel->getAllAdmin();
           $this->load->view('admin/assets/header',$data);  
           $this->load->view('admin/assets/sidebar',$data);
           $this->load->view('admin/about/add',$data);  
           $this->load->view('admin/assets/footer',$data);

   }
   
   function edit($id){
       
       $this->load->helper(array('form', 'url'));
       $this->load->library(array('session','pagination'));
       $this->load->model('aboutmodel','',TRUE);     
      
       $data = array(
         'header' => $this->load->view('admin/assets/header', '', TRUE), 
          'sidebar' =>$this->load->view('admin/assets/sidebar',''.TRUE),   
         'footer' => $this->load->view('admin/assets/footer', '', TRUE),
        );
       $data['record']=$this->aboutmodel->fetch_details($id);
       if(!empty($this->input->post())){
          $result=$this->aboutmodel->edit($id,$this->input->post());
          if($result){
            self::upload_file($id,$_FILES,'image1','edit'); 
            self::upload_file($id,$_FILES,'image2','edit'); 
            self::upload_file($id,$_FILES,'image3','edit');    
          }
        //   $sql=$this->db->query('INSERT into about_logs (about_id) values('.$id.')');
          $this->session->set_flashdata('message',"About has been updated.");
          redirect("admin/about");
       }
           
           $data['values']=$this->aboutmodel->getAllAdmin();
           $this->load->view('admin/assets/header',$data);  
           $this->load->view('admin/assets/sidebar',$data);
           $this->load->view('admin/about/edit',$data);
           $this->load->view('admin/assets/footer',$data);
   }
     function upload_file($id,$fileArray,$column,$for='add'){
      if($id!='' && !empty($fileArray) && $fileArray[$column]['error']=="0" && $column!=''){
          
            $record=$this->aboutmodel->fetch_details($id);
            $ext = pathinfo($fileArray[$column]['name'], PATHINFO_EXTENSION);
            $file_name=rand(10,1000).date('YmdHis').".".$ext;
            if($for=="edit"){
                @unlink(FCPATH."uploads/about/".$record->$column);
            }
            $ffdsf=$this->aboutmodel->edit($id,[$column=>$file_name]);
            move_uploaded_file($fileArray[$column]['tmp_name'],FCPATH."uploads/about/".$file_name);
            chmod($ffdsf,0777);
            return true;
       }
   }
   
   function delete($id){
       $this->load->helper(array('form', 'url'));
       $this->load->library(array('session','pagination'));
       $this->load->model('aboutmodel','',TRUE);     
       if($id!=''){
         $this->aboutmodel->deleteabout($id);
         $this->session->set_flashdata('message',"About has been deleted.");
         redirect("admin/about");  
       }
   }
   // function view($id){

   //     $this->load->helper(array('form', 'url'));
   //     $this->load->library(array('session','pagination'));
   //     $this->load->model('aboutmodel','',TRUE);     
   //     $data = array(
   //       'header' => $this->load->view('admin/assets/header', '', TRUE),  
   //       'footer' => $this->load->view('admin/assets/footer', '', TRUE),
   //      );
   //     $data['record']=$this->aboutmodel->fetch_details($id);
   //     if(!empty($this->input->post())){
   //        $result=$this->aboutmodel->edit($id,$this->input->post());
   //        if($result){
   //          self::upload_file($insert_id,$_FILES,'image','edit');    
   //        }
   //        $this->session->set_flashdata('message',"about has been updated.");
   //        redirect("admin/about");
   //     }
       
   //    $this->load->view('admin/about/view',$data);
   // }
   
    function update_status(){
        
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session','pagination'));
        $this->load->model('aboutmodel','',TRUE);    
        
        $id= $_REQUEST['sid'];
        $status= $_REQUEST['svalue'];
        if($status == 'active')
        {
        $sql=$this->db->query("UPDATE about set status='inactive' where id='$id'");
        }else{
    
        $sql=$this->db->query("UPDATE about set status='active' where id='$id'");

    }
    redirect("admin/about");
  }
  

}