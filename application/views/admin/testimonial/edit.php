<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit Testimonial</h1>
        </div>

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Edit Testimonial</li>
          </ol>
        </div>
      </div>
    </div>

  </section>

  <!-- Main content -->
  <section class="content">
    <form role="form" id="Form1" method="post" enctype="multipart/form-data" action="">
      <div class="row">
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-body">
            <div class="form-group">
                <label for="image">Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Name" id="name" value="<?php echo $record->name; ?>" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="address">Description </label>
                <textarea name="description" class="form-control sumernote" id="description" placeholder="Enter Description"> <?php echo $record->description;?> </textarea>
              </div>
            
              <div class="box-body">
              <div class="form-group">
                <label for="image">Testimonial Image</label>
                <input type="file" name="image" class="form-control" id="image" >
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>
              
            </div>
            <div class="inner_card">
            <div class="card_heading">
              <label for="">Image</label>
            </div>
            <div class="card_content">
       
            <div class="image_preview">
              <?php
              if (isset($record->image) && file_exists(FCPATH . "uploads/testimonial/" . $record->image) && $record->image != '') {
                echo '<img src="' . base_url() . 'uploads/testimonial/' . $record->image . '" width="100px">';
              }
              ?>
          
            </div>
          </div>
          
        </div>
          </div>
        </div>
      
      <div class="col-md-3">

        <div class="inner_card">
          <div class="card_heading">
            <label for="">PUBLISH</label>
          </div>
          <div class="card_content">
            <button type="submit" name="sbt" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </div>
    </form>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="http://dev.soulilution.com/peertopia/Backend/additional-methods.min.js"></script>