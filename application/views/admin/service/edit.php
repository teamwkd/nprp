<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-2">
          <h1>Edit Service</h1>
        </div>
        <div class="col-sm-3">
          <a  href="<?php echo base_url(); ?>admin/service/add" class="add_new" ><i class="fas fa-plus-circle"></i> Add New</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Edit Service</li>
          </ol>
        </div>
      </div>
    </div>

  </section>

  <!-- Main content -->
  <section class="content">

        <!-- general form elements -->
        
         
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" id="Form1" method="post" enctype="multipart/form-data" action="">
            <div class="box-body">
              <div class="content">
                <div class="row">
                  <div class="col-md-9">
                  <div class="box box-primary">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Service Image</label>
                      <input type="file" name="image" class="form-control" id=image">
                      <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
                    </div>
                    <div class="form-group">
                <label for="image">Title</label>
                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="<?php echo $record->title ?>" id="title" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Service Name</label>
                <input type="text" name="service_name" class="form-control" placeholder="Enter Service Name" value="<?php echo $record->service_name ?>" id="service_name" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="address">Service Description </label>
                <textarea name="service_description" class="form-control sumernote" id="service_description" placeholder="Enter Featured Description"><?php echo $record->service_description ?></textarea>
            </div>

                 
                  </div>
                  </div>
                 
                  <div class="col-md-3">
                    <div class="inner_card">
                      <div class="card_heading">
                        <label for="">Service Image</label>
                      </div>
                      <div class="card_content">
                 
                      <div class="image_preview">
                        <?php
                        if (isset($record->image) && file_exists(FCPATH . "uploads/service/" . $record->image) && $record->image != '') {
                          echo '<img src="' . base_url() . 'uploads/service/' . $record->image . '" width="100px">';
                        }else{?>
                        <p>NA</p>
<?php
                        }
                        ?>
                    
                      </div>

                      
                    </div>
                  <div class="inner_card">
                      <div class="card_heading">
                        <label for="">PUBLISH</label>
                      </div>
                      <div class="card_content">
                        <button type="submit" name="sbt" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                </div>
              </div>

            </div>
            <!-- /.box-body -->

           
          </form>
      
   

    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="http://dev.soulilution.com/peertopia/Backend/additional-methods.min.js"></script>

<script type="text/javascript" src="https://www.imtech.res.in/js/jquery.tokeninput.js"></script>
<link rel="stylesheet" type="text/css" href="https://www.imtech.res.in/adminWebroot/styles/token-input.css" />


<script>
  $(document).ready(function() {
    $("#Form1").validate({

      rules: {
        'name': {
          required: true
        },
        'address': {
          required: true
        },
        'email': {
          required: true,
          email: true
        },
        'image': {
          extension: 'doc|pdf|jpg|jpeg|docx|png'
        },
      }
    });
  });

  $(document).ready(function() {
    $("#brand_name").tokenInput('<?php echo base_url(); ?>admin/vendor/search_brand'
      <?php if (!empty($brand_selected)) { ?>, {
          prePopulate: <?php echo json_encode($brand_selected); ?>
        }
      <?php } ?>);
  });
  $(document).ready(function() {
    $("#item_name").tokenInput('<?php echo base_url(); ?>admin/vendor/search_item'
      <?php if (!empty($item_selected)) { ?>, {
          prePopulate: <?php echo json_encode($item_selected); ?>
        }
      <?php } ?>);
  });
  $(document).ready(function() {
    $("#supplier_name").tokenInput('<?php echo base_url(); ?>admin/vendor/search_vendor'
      <?php if (!empty($vendor_selected)) { ?>, {
          prePopulate: <?php echo json_encode($vendor_selected); ?>
        }
      <?php } ?>);
  });
</script>
<style>
  .form-group {
    margin-bottom: 15px;
    width: 97%;
  }

  textarea {
    height: 33px;
  }

  li.token-input-token {
    padding: 5px 8px !important;
    background-color: #1d77b5 !important;
    color: #fff !important;
    display: inline-block !important;
    float: left;
  }

  li.token-input-token span {
    color: #fff !important;
    font-size: 14px;
    margin-left: 11px;
  }

  ul.token-input-list {
    width: 100% !important
  }

  ul.token-input-list li input {
    border: 0;
    width: auto !important;
  }

  li.token-input-selected-token {
    background-color: #f59436 !important;
    color: #fff !important;
  }

  li.token-input-selected-token {
    background-color: #f59436 !important;
    color: #fff !important;
  }

  li.token-input-selected-token span {
    font-size: 15px;
    margin-left: 13px;
    color: #fff !important;
  }
</style>