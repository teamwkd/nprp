 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url();?>admin/dashboard" class="brand-link">
      <img src="<?php echo base_url();?>assets/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>assets/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?php echo base_url();?>admin/dashboard" class="d-block"><?php echo $values[0]->name;?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
          <?php
          $get_controller=$this->router->fetch_class();
          $get_method=$this->router->fetch_method();
          ?>
           
          </li>
          <li class="nav-item has-treeview">
          <a href="<?php echo base_url(); ?>admin/dashboard" class="nav-link">
          <i class="fas fa-shield-alt nav-icon"></i>
            <p>
            Dashboard
              <i class="fas fa-angle-left right"></i>
              <!-- <span class="badge badge-info right">6</span> -->
            </p>
          </a>
        
        </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fa fa-home nav-icon" aria-hidden="true"></i>
              <p>
              Manage About 
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
          <ul class="nav nav-treeview">
             <li class="nav-item">
              <a href="<?php echo base_url();?>admin/about" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage About List</p>
              </a>
            </li>
           
          </ul>
        </li>


        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fa fa-home nav-icon" aria-hidden="true"></i>
              <p>
              Manage Testimonial 
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
          <ul class="nav nav-treeview">
             <li class="nav-item">
              <a href="<?php echo base_url();?>admin/testimonial" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Manage Testimonial List</p>
              </a>
            </li>
           
          </ul>
        </li>


        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon  fab fa-product-hunt"></i>
            <p>
              Manage Service 
              <i class="fas fa-angle-left right"></i>
              <!-- <span class="badge badge-info right">6</span> -->
            </p>
          </a>
          <ul class="nav nav-treeview">
            
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/service" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Service List</p>
              </a>
            </li>
            
          </ul>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon  fab fa-product-hunt"></i>
            <p>
              Manage Team 
              <i class="fas fa-angle-left right"></i>
              <!-- <span class="badge badge-info right">6</span> -->
            </p>
          </a>
          <ul class="nav nav-treeview">
            
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/team" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Team List</p>
              </a>
            </li>
            
          </ul>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon  fab fa-product-hunt"></i>
            <p>
              Manage Contact 
              <i class="fas fa-angle-left right"></i>
              <!-- <span class="badge badge-info right">6</span> -->
            </p>
          </a>
          <ul class="nav nav-treeview">
            
            <li class="nav-item">
              <a href="<?php echo base_url();?>admin/contact" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Contact List</p>
              </a>
            </li>
            
          </ul>
        </li>

      </ul>
    </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>