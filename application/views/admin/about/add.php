<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Add About</h1>
        </div>

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Add About</li>
          </ol>
        </div>
      </div>
    </div>

  </section>

  <!-- Main content -->
  <section class="content">


    <form role="form" id="Form1" method="post" enctype="multipart/form-data" action="">
      <div class="row">
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-body">
            <div class="form-group">
                <label for="image">Featured Heading</label>
                <input type="text" name="heading1" class="form-control" placeholder="Enter Featured Heading" id="heading1" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Second Heading</label>
                <input type="text" name="heading2" class="form-control" placeholder="Enter Second Heading" id="heading2" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Third Heading</label>
                <input type="text" name="heading3" class="form-control" placeholder="Enter Third Heading" id="heading3" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="address">Featured Description </label>
                <textarea name="description1" class="form-control sumernote" id="description" placeholder="Enter Featured Description"></textarea>
              </div>
              <div class="form-group">
                <label for="address">Second Description </label>
                <textarea name="description2" class="form-control sumernote" id="description" placeholder="Enter Second Description"></textarea>
              </div>
              <div class="form-group">
                <label for="address">Third Description </label>
                <textarea name="description3" class="form-control sumernote" id="description" placeholder="Enter Third Description"></textarea>
              </div>
              <div class="box-body">
              <div class="form-group">
                <label for="image">Featured Image</label>
                <input type="file" name="image1" class="form-control" id="image1" required>
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>

            <div class="box-body">
              <div class="form-group">
                <label for="image">Second Image</label>
                <input type="file" name="image2" class="form-control" id="image2" required>
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>

             <div class="box-body">
              <div class="form-group">
                <label for="image">Third Image</label>
                <input type="file" name="image3" class="form-control" id="image3" required>
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>
              
            </div>

          </div>
        </div>
      
      <div class="col-md-3">

        <div class="inner_card">
          <div class="card_heading">
            <label for="">PUBLISH</label>
          </div>
          <div class="card_content">
            <button type="submit" name="sbt" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </div>
    </form>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="http://dev.soulilution.com/peertopia/Backend/additional-methods.min.js"></script>