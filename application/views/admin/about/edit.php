<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit About</h1>
        </div>

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Edit About</li>
          </ol>
        </div>
      </div>
    </div>

  </section>

  <!-- Main content -->
  <section class="content">
    <form role="form" id="Form1" method="post" enctype="multipart/form-data" action="">
      <div class="row">
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-body">
            <div class="form-group">
                <label for="image">Featured Heading</label>
                <input type="text" name="heading1" class="form-control" placeholder="Enter Featured Heading" id="heading1" value="<?php echo $record->heading1; ?>" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Second Heading</label>
                <input type="text" name="heading2" class="form-control" placeholder="Enter Second Heading" id="heading2" value="<?php echo $record->heading2; ?>" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Third Heading</label>
                <input type="text" name="heading3" class="form-control" placeholder="Enter Third Heading" id="heading3" value="<?php echo $record->heading3; ?>" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>


              <div class="form-group">
                <label for="address">Featured Description </label>
                <textarea name="description1" class="form-control sumernote" id="description1" placeholder="Enter Featured Description"> <?php echo $record->description1;?>  </textarea>
              </div>
              <div class="form-group">
                <label for="address">Second Description </label>
                <textarea name="description2" class="form-control sumernote" id="description2" placeholder="Enter Second Description"> <?php echo $record->description2;?> </textarea>
              </div>
              <div class="form-group">
                <label for="address">Third Description </label>
                <textarea name="description3" class="form-control sumernote" id="description3" placeholder="Enter Third Description"> <?php echo $record->description3;?> </textarea>
              </div>

              <div class="box-body">
              <div class="form-group">
                <label for="image">About Image 2</label>
                <input type="file" name="image2" class="form-control" id="image" >
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>
            <div class="inner_card">
            <div class="card_heading">
              <label for="">About Image 2</label>
            </div>
            <div class="card_content">
            <div class="image_preview">
              <?php
              if (isset($record->image2) && file_exists(FCPATH . "uploads/about/" . $record->image2) && $record->image2 != '') {
                echo '<img src="' . base_url() . 'uploads/about/' . $record->image2 . '" width="100px">';
              }
              ?>
          
            </div>
          </div>
          
        </div>
        <div class="box-body">
              <div class="form-group">
                <label for="image">About Image 3</label>
                <input type="file" name="image3" class="form-control" id="image3" >
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>
            <div class="inner_card">
            <div class="card_heading">
              <label for="">About Image 3</label>
            </div>
            <div class="card_content">
            <div class="image_preview">
              <?php
              if (isset($record->image3) && file_exists(FCPATH . "uploads/about/" . $record->image3) && $record->image3 != '') {
                echo '<img src="' . base_url() . 'uploads/about/' . $record->image3 . '" width="100px">';
              }
              ?>
          
            </div>
          </div>
          
        </div>
</div>

        </div>
        </div>
      
      <div class="col-md-3">

        <div class="inner_card">
          <div class="card_heading">
            <label for="">PUBLISH</label>
          </div>
          <div class="card_content">
            <button type="submit" name="sbt" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </div>
    </form>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="http://dev.soulilution.com/peertopia/Backend/additional-methods.min.js"></script>