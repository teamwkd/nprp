
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-2">
          <h1>Team List</h1>
        </div>
        <div class="col-sm-3">
          <a   href="<?php echo base_url();?>admin/team/add"class="add_new" ><i class="fas fa-plus-circle"></i> Add New</a>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Team</li>
          </ol>
        </div>
      </div>
    </div>

  </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body bg_white">
            <h4 class="card-title">View Details</h4>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <?php if( $this->session->flashdata('message') !='' ){ ?>
                     <tr><td colspan="4" class="err_msg"><?php echo $this->session->flashdata('message'); ?></td></tr>
                <?php }?> 
                <tr>
                  <th>SNO</th>
                  <th>Image</th>
                  <th>Employee Name</th>
                  <th>Employee Designation</th>
                  <th>Status</th>
                  <th style="width: 230px;">Action</th>
                  
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($content_array)){
                    $i=1;
                    foreach($content_array as $data){
                    ?>
                 

                <tr>
                  <td><?php echo $i;?></td>
                  <td>
                  <?php if($data->image!=''){?>
                    <img src="<?php echo base_url();?>uploads/team/<?php echo $data->image;?>" class="small_img">
                  <?php }else{
                    echo "NA";
                  } ?>
                 </td>
                 <td><?php echo $data->name; ?></td>
                 <td><?php echo $data->designation; ?></td>
                 <td> 
                  <?php $status = $data->status; 
                  if ($status == 'active') {?> 
                      <a title="Click to Deactivate" href="<?php echo base_url();?>admin/team/update_status?sid=<?php echo $data->id;?>&svalue=<?php echo $status?>" class="btn btn success"><i class="fas fa-thumbs-up"></i>&nbsp;Active</a> 
                  <?php } else {?> 
                      <a title="Click to Activate" href="<?php echo base_url();?>admin/team/update_status?sid=<?php echo $data->id;?>&svalue=<?php echo $status?>" class="btn btn-danger"><i class="fas fa-thumbs-down"></i> &nbsp;Inactive</a> 
                  <?php } ?>
                 </td> 
                  <td>
                    <a class="edit" href="<?php echo base_url();?>admin/team/edit/<?php echo $data->id;?>"><i class="far fa-edit"></i>&nbsp;EDIT</a>
                    <a class="delete delete_record" href="javascript:" data-record-id="<?php echo $data->id;?>"><i class="fas fa-eye"></i> &nbsp;DELETE</a>
                  </td>
                </tr>
                <?php $i++;} } ?>
                <!-- <tr><td colspan="5"><div class="pagination"><?php echo $pagination_links;?></div></td></tr> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script type=""> 
  $(function(){
     $('.delete_record').on('click',function(){
        var record_id=$(this).data('record-id');
        $('.delete_button').attr('data-delete-id',record_id);
        $('#deleteModal').modal('show');
        
     });
     $('.delete_button').on('click',function(){
        var record_id=$(this).data('delete-id');
        var location = "<?php echo urldecode(base_url()."admin/Team/delete"); ?>/"+record_id;     
        window.location.href=location;
     });
     
  });
  </script>
  
  
<div class="example-modal">
        <div class="modal modal-danger" id="deleteModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Team</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure want to delete.?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                <button type="button" data-delete-id='' class="btn btn-outline delete_button">Delete</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
</div>  
