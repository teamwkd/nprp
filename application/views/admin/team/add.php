<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Add Team</h1>
        </div>

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li>Home Page</li>
            <li>/</li>
            <li class="active">Add Team</li>
          </ol>
        </div>
      </div>
    </div>

  </section>

  <!-- Main content -->
  <section class="content">


    <form role="form" id="Form1" method="post" enctype="multipart/form-data" action="">
      <div class="row">
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-body">
            <div class="form-group">
                <label for="image">Employee Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Name" id="name" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
              <div class="form-group">
                <label for="image">Employee Designation</label>
                <input type="text" name="designation" class="form-control" placeholder="Enter Designation" id="designation" required>
                <!-- <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div> -->
              </div>
            
              <div class="box-body">
              <div class="form-group">
                <label for="image">Featured Image</label>
                <input type="file" name="image" class="form-control" id="image" required>
                <div class="bg_gray"><span>Allowed file type is jpg | jpeg | png. Maximum Allowed file size is 100kb. File Dimensions (1017×380)</span></div>
              </div>
            </div>
          </div>

          </div>
        </div>
      
      <div class="col-md-3">

        <div class="inner_card">
          <div class="card_heading">
            <label for="">PUBLISH</label>
          </div>
          <div class="card_content">
            <button type="submit" name="sbt" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </div>
    </form>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="http://dev.soulilution.com/peertopia/Backend/additional-methods.min.js"></script>