<section class="bredcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2><?php echo $record[0]['title'];?></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="service_inner_page_wrap">
    <div class="container">
        <div class="row">
           
            <div class="col-md-7">
                <div class="tab-content">
                   <?php echo $record[0]['service_description']; ?>
                </div>
            </div>

            <div class="col-md-5">
              <div class="service_inner_wrap_layout">
                <div class="right_bg">
                <img src="<?php echo base_url();?>/uploads/service/<?php echo $record[0]['image'];?>" alt="">
                </div>
                <div class="service_right_quick_access">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php if(count($service)>0){
        foreach($service as $serv){?>
                        <li class="nav-item">
                            <a class="nav-link active" id="audit-tab" data-toggle="tab" href="<?php echo base_url();?>service/service-details/<?php echo $serv['id']; ?>"" role="tab"
                                aria-controls="audit" aria-selected="true"><?php echo $serv['title']; ?></a>
                        </li>
        <?php }} ?>
                    </ul>
                </div>
              </div>      
            </div>
        </div>
    </div>
</section>
