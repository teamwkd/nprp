
<section class="about_breadcumb contact_bredcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2>Contact Us</h2>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="contact_wrpaper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact_map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.098961830843!2d77.23960481492323!3d28.626795982420074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfcd53f3ead11%3A0x3262caaa8f08db86!2sNPRP%20%26%20Co.!5e0!3m2!1sen!2sin!4v1579095028310!5m2!1sen!2sin"
                        width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="col-md-5">
                <div class="contact_details_wrap">
                    <h4>We’re Happy to DiscussYour Project and Answer any Question</h4>
                    <ul>
                        <li><i class="ti-location-pin"></i><span>Address</span></li>
                        <p>55 Ground Floor, Bapu Park,Kotla Mubarkh Pur,New-Delhi-1100036</p>
                    </ul>
                    <ul>
                        <li><i class="fas fa-phone"></i><span>Contact Number</span></li>
                        <p><a href="tel:+91-9971014147">+91-9971014147</a></p>
                    </ul>
                    <ul>
                        <li><i class="far fa-envelope"></i><span>Email</span></li>
                        <p><a href="maito:">info@nprp.com</a></p>
                    </ul>
                </div>
            </div>
            <div class="col-md-7">
                <div class="contac_form_box_wrap">
                    <div class="contact_form_inner">
                        <h3>Let’s Start  The Conversation.</h3>
                        <form action="<?php echo base_url();?>home/addContact" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form_group_crt">
                                        <input name="username" type="text" placeholder="Name" id="username">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form_group_crt">
                                        <input name="email" type="email" placeholder="Email" id="email" class="color_input">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form_group_crt">
                                        <input name="mobile" type="tel" placeholder="Mobile Number" id="mobile">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form_group_crt">
                                        <input name="subject" type="text" placeholder="Subject" id="subject">
                                    </div>
                                </div>
                                 <div class="col-md-12">
                                    <div class="form_group_crt">
                                        <textarea name="message" rows="4" placeholder="Message" id="message" ></textarea>     
                                    </div>
                                 </div>
                                 <div class="text_left col-md-12">
                                    <button name="sbt" type="submit" value="Submit" class="site_button site_btn_effect">
                                            Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>