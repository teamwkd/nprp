
<section class="bredcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2>Our Team</h2>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="team_section">
    <div class="container">
        <?php if(count($member)>0){
        foreach($member as $team){
        ?>
        <div class="row no-gutters mt_botom">
            <div class="col-md-3">
                <div class="team_inner_wrpa">
                    <div class="teams_bg">
                        <img src="<?php echo base_url();?>uploads/team/<?php echo $team['image']; ?>" alt="">
                    </div>
                    <div class="team_bg_sec_title">
                        <h5><?php echo $team['name']; ?></h5>
                        <p><?php echo $team['designation']; ?></p>
                    </div>
                </div>
            </div>
           
        </div>
       <?php }} ?>
    </div>
</section>

