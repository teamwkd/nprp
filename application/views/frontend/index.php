
<section class="home_slider">
  <div class="owl-carousel owl-theme hero_slider">
    <div class="item">
      <div class="hero_slider_inner" style="background-image: url(assets/img/slider_bg1.jpg);">
        <div class="hero_slider_contnet desc1">
          <span>Helping You to</span>
          <h4>We Provide Financial Planning</h4>
          <p>Comprehensive financial advice and financial services tha are tailored to meet your individual needs. </p>
          <div class="hero_slider_buutons">
            <a href="#" class="hero_brn1">Our Services</a>
            <a href="#" class="hero_brn2">Quick Call Back</a>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="hero_slider_inner" style="background-image: url(assets/img/slider_bg2.jpg);">
        <div class="hero_slider_contnet desc1">
          <span>Helping You to</span>
          <h4>Expert Consultants and Investment</h4>
          <p>Create a refreshing customer experience that's free of convexity and confusion. </p>
          <div class="hero_slider_buutons">
            <a href="#" class="hero_brn1">Our Services</a>
            <a href="#" class="hero_brn2">Quick Call Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="about_sec_content">
          <div class="bg_symbol">A</div>
          <span>ABOUT NPRP</span>
          <h1>We work with you to address your most critical business priorities</h1>
          <p>
            We focus on delivering quantifiable results for our customers, based on a well-tested methodology and solid
            experience. But of course, this only works because of our close business, taking time to applying our
            expertise and transferring skills within your own organisation.
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis corrupti tempora, ab maxime atque at
            adipisci voluptate nisi officia assumenda aspernatur? Minus ducimus soluta, reiciendis ea tempore odio eius?
            Veniam.
          </p>
          <div class="about_sec_btn">
            <a href="<?php echo base_url();?>about">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="about_bg_sec">
          <img src="assets/img/about_bg.png" alt="">
          <div class="about_bg_sec_inner">
            <div class="about_sec_icon">
              <i class="ti-world"></i>
            </div>
            <div class="about_sec_expreincs">
              <span class="counter" data-count="150"></span>
              <p>Years Of Experience</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="service_offers">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="service_offer_title">
          <div class="bg_symbol lef_adjust">S</div>
          <span> OUR SERVICES </span>
          <h2>What We Offer for You</h2>
        </div>
      </div>
      <div class="col-md-3">
        <div class="service_btn_stly">
          <a href="<?php echo base_url();?>service">View All Services</a>
        </div>
      </div>
    </div>
    <div class="row">
    <?php if(count($service)>0){
        foreach($service as $serv){?>
      <div class="col-md-4">
        <div class="service_offer_box">
          <div class="service_Offer_box_inner">
            <img src="<?php echo base_url();?>uploads/service/<?php echo $serv['image'];?>" alt="">
          </div>
          <div class="service_offer_box_content_wrap">
            <div class="service_content_box_desc">
              <h4><?php echo $serv['title']; ?></h4>
              <p><?php echo $serv['service_name']; ?></p>
              <a href="<?php echo base_url();?>service/service-details/<?php echo $serv['id']; ?>" class="moredn_btn">
                Learn More
                <div class="strips"></div>
                <i class="ti-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <?php }} ?>
    </div>
  </div>
</section>

<section class="extra_bootom_sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="extra_bottom_sec_inner">
          <span>WE MAKE A DIFFERENCE</span>
          <h3>The Best Solutions For Developing Your Business</h3>
          <a href="<?php echo base_url();?>contact">Get Our Services<i class="fas fa-long-arrow-alt-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="why_chose">
  <div class="container">
    <div class="why_choose_us_wrap">
      <div class="why_choose_us_bg">
        <img src="<?php echo base_url();?>assets/img/why_choose_bg.jpg" alt="">
      </div>
      <div class="why_choose_us_content_wrap">
        <div class="why_choose_desc_inner">
          <div class="bg_symbol lef_third_adjust">W</div>
          <span>Why Choose Us</span>
          <h5>We creating solutionsfor your organization & Business</h5>
        </div>
        <div class="why_choose_desc_boxs border_btm">
          <div class="why_choose_icon">
            <i class="flaticon-insurance"></i>
          </div>
          <div class="why_choose_right_content">
            <h6>Protect your business</h6>
            <p>Avoid Revealing Apply for, Provisional Patent, Trademark Your Name, Follow Your Instincts.</p>
          </div>
        </div>
        <div class="why_choose_desc_boxs border_btm">
          <div class="why_choose_icon">
            <i class="flaticon-data-analysis"></i>
          </div>
          <div class="why_choose_right_content">
            <h6>Optimize Your Systems</h6>
            <p>By dealing with data, select tools that help you automate time-consuming processes.</p>
          </div>
        </div>
        <div class="why_choose_desc_boxs border_btm">
          <div class="why_choose_icon">
            <i class="flaticon-business"></i>
          </div>
          <div class="why_choose_right_content">
            <h6>Budget Friendly</h6>
            <p>By dealing with data, select tools that help you automate time-consuming processes.</p>
          </div>
        </div>
        <div class="why_choose_desc_boxs">
          <div class="why_choose_icon">
            <i class="flaticon-chat"></i>
          </div>
          <div class="why_choose_right_content">
            <h6>Valuable ideas</h6>
            <p>Strategic initiatives – all ideas welcome rather it’s by our employees or client.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="testimonial">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="testimonila_left_wrap">
          <div class="bg_symbol left_secont_adjust">T</div>
          <span>Testimonial</span>
          <h6>What Our Clients Say About Us</h6>
          <p>Nullam porttitor nulla et risus tempor, eu aliquet dui scelerisque. Vestibulum quis dignissim velit, ac
            faucibus ligula. Morbi convallis arcu in lorem.</p>
          <div class="testimonial_left_bottom">
            <i class="fas fa-quote-right"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="testimonial_wrap_layout">
          <div class="owl-carousel owl-theme testimonial_slider">
            <?php 
            foreach($testimon  as $test){
            ?>
            <div class="item">
              <div class="testimonial__wrap__info">
                <div class="testimonial__inner_wrap_content">
                  <h6><?php echo $test['quote']; ?></h6>
                  <p><?php echo $test['description']; ?></p>
                </div>
                <div class="extra___testimonial">
                  <div class="testimonial_image">
                    <img src="<?php echo base_url();?>uploads/testimonial/<?php echo $test['image']; ?>" alt="client-img">
                  </div>
                  <div class="clients_details">
                    <h6><?php echo $test['name']; ?></h6>
                    <p><?php echo $test['designation']; ?></p>
                  </div>
                </div>
              </div>
            </div>
        <?php } ?>
          </div> 
        </div>
      </div>
    </div>
  </div>
</section>


<section class="team_sections">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="team_sec_title">
          <div class="bg_symbol lef_adjust">O</div>
          <span>OUR TEAM</span>
          <h6>Meet Our Best Team</h6>
        </div>
      </div>
  
      <div class="owl-carousel owl-theme team_sec_slider">
         <?php 
         if(count($member)>0){
           foreach($member as $team){
         ?>
        <div class="item">
          <div class="each_team_sec">
            <div class="team_sec_bg">
              <img src="<?php echo base_url();?>uploads/team/<?php echo $team['image']; ?>" alt="">
            </div>
            <div class="team_sec_tiles">
              <h5><?php echo $team['name']; ?></h5>
              <p><?php echo $team['designation']; ?></p>
            </div>
          </div>
        </div>
           <?php }} ?>
      </div>
      <div class="col-md-6">
        <div class="team_sec_bootm_lef">
          <h6> Want to Join Our Team?</h6>
          <p>Pellentesque tempor ornare mal esu.</p>
        </div>
      </div>
      <div class="col-md-6">
        <a href="<?php echo base_url(); ?>contact" class="moredn_btn ft_left">
          Contact Us
          <div class="strips"></div>
          <i class="ti-arrow-right"></i>
        </a>
      </div>
    </div>
  </div>
</section>

<section class="blog">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="blogs_title">
          <div class="bg_symbol">N</div>
          <span>NEWS & BLOGS</span>
          <h6>Latest News & Blogs </h6>
        </div>
      </div>
      <div class="col-md-4">
        <div class="blog_sec_btn">
          <a href="#">View More</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="blog_sec_wrap">
          <a href="#">
            <div class="blog_sec_img">
              <img src="assets/img/blog1.jpg" alt="">
            </div>
          </a>
          <div class="blog_sec_wrap_content">
            <p class="blue_title">
              <span>30 JUL 2019 - BY ADMIN</span>
            </p>
            <h5>Nullam lobortis iaculis tortor, eu mollis eros au..</h5>
            <a href="#">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="blog_sec_wrap">
          <a href="#">
            <div class="blog_sec_img">
              <img src="assets/img/blog1.jpg" alt="">
            </div>
          </a>
          <div class="blog_sec_wrap_content">
            <p class="blue_title">
              <span>30 JUL 2019 - BY ADMIN</span>
            </p>
            <h5>Nullam lobortis iaculis tortor, eu mollis eros au..</h5>
            <a href="#">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="top__foot">
  <div class="container custom__container">
    <div class="row align-items-center">
      <div class="col-md-5">
        <div class="news__letter_content">
          <h6>Subscribe Our Newsletter</h6>
          <p>Luisque dapibus lacus non pulvinar lobo.</p>
        </div>
      </div>
      <div class="col-md-7">
        <form action="#">
          <input class="form__control_news" placeholder="Enter your email" type="email">
          <button type="submit" class="btn newsletter__btn_sumbit blob-small">Subscribe</button>
        </form>
      </div>
    </div>
  </div>
</section>
