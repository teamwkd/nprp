
<section class="about_breadcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2>About Us</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about_title">
                    <h1>Our Promise</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deleniti nostrum dolor, accusamus asperiores earum recusandae quas fugiat voluptates, laborum eveniet repudiandae error harum quisquam maiores. Architecto amet officia ea incidunt! Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maiores cum dolores amet iste neque quam eum deserunt consequuntur praesentium, aperiam, atque aut suscipit voluptates vero aspernatur earum dolorum a nihil.</p>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="about_wrap_box">
                    <div class="about_inner_wrap">
                        <div class="card_contnet">
                            <div class="about_icon">
                                <img src="assets/img/about_icon2.png" alt="">
                            </div>
                            <div class="icon_title">
                                <h4>Leading the Industry</h4>
                            </div>
                            <div class="about_sub_title">
                               <p> We helped pioneer long term care insurance over 40 years ago and we’re still leading the industry today.</p>
                            </div>
                            <div class="about_card_btn">
                                <a href="<?php echo base_url(); ?>service" class="moredn_btn">
                                    View Service
                                   <div class="strips"></div>
                                   <i class="ti-arrow-right"></i>
                                 </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about_wrap_box">
                    <div class="about_inner_wrap">
                        <div class="card_contnet">
                            <div class="about_icon">
                                <img src="<?php echo base_url();?>assets/img/about_icon.png" alt="">
                            </div>
                            <div class="icon_title">
                                <h4>Join Us</h4>
                            </div>
                            <div class="about_sub_title">
                               <p> We helped pioneer long term care insurance over 40 years ago and we’re still leading the industry today.</p>
                            </div>
                            <div class="about_card_btn">
                                <a href="<?php echo base_url();?>contact" class="moredn_btn">
                                     Contact Us
                                   <div class="strips"></div>
                                   <i class="ti-arrow-right"></i>
                                 </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about_wrap_box">
                    <div class="about_inner_wrap">
                        <div class="card_contnet">
                            <div class="about_icon">
                                <img src="<?php echo base_url();?>assets/img/about_icon3.png" alt="">
                            </div>
                            <div class="icon_title">
                                <h4>Social Responsibility</h4>
                            </div>
                            <div class="about_sub_title">
                               <p> We helped pioneer long term care insurance over 40 years ago and we’re still leading the industry today.</p>
                            </div>
                            <div class="about_card_btn">
                                <a href="<?php echo base_url();?>contact" class="moredn_btn">
                                    Contact Us
                                    <div class="strips"></div>
                                    <i class="ti-arrow-right"></i>
                                  </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="what_we">
                      <div class="row no-gutters">
                          <div class="col-md-7">
                              <div class="what_we_do_wrap">
                                     <div class="what_we_content">
                                         <h4>About Us Company</h4>
                                         <p>
                                             Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id quis sapiente blanditiis voluptas at temporibus doloremque, fugiat fuga! Possimus fugit sint facilis cupiditate cum corporis amet officiis veniam dolores quibusdam.
                                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur voluptatem ut id quod optio doloremque rem error, et eum tempora ullam eos doloribus possimus ipsum voluptas natus impedit voluptate? Optio.
                                         </p>
                                     </div>
                              </div>
                          </div>
                          <div class="col-md-5">
                              <div class="what_we_do_bg">
                              </div>
                          </div>
                      </div>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="what_we">
                      <div class="row no-gutters">
                        <div class="col-md-5">
                            <div class="what_we_do_bg bg_chnage">
                            </div>
                        </div>
                          <div class="col-md-7">
                              <div class="what_we_do_wrap">
                                     <div class="what_we_content">
                                         <h4>What We Offer</h4>
                                         <p>
                                             Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id quis sapiente blanditiis voluptas at temporibus doloremque, fugiat fuga! Possimus fugit sint facilis cupiditate cum corporis amet officiis veniam dolores quibusdam.
                                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur voluptatem ut id quod optio doloremque rem error, et eum tempora ullam eos doloribus possimus ipsum voluptas natus impedit voluptate? Optio.
                                         </p>
                                     </div>
                              </div>
                          </div>
                      </div>
                </div>  
            </div>
        </div>
    </div>
</section>

