<?php include('inc/header.php')?>
<section class="bredcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2>Good and Services Tax(GST)</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="service_inner_page_wrap">
    <div class="container">
        <div class="row">  
            <div class="col-md-7">
                <div class="tab-content">
                <div class="tab-pane active" id="goods" role="tabpanel" aria-labelledby="goods-tab">
                        <div class="tab_contnets_inner_box">
                        <h3>Goods and Services Tax (GST)</h3>
                        <p>We NPRP & Co; are providing various services under GST laws. Some of them are:  </p>
                         <ul>
                             <li>GST Audit</li>
                             <li>GST Registration</li>
                             <li>Filing GST Returns</li>
                             <li>Accounting for GST Return Filing Purpose</li>
                             <li>Assisting in getting GST Refund</li>
                             <li>Attending to the Notices issued under GST Law</li>
                             <li>Filing and Pleading Appeals wherever necessary</li>
                             <li>GST Consultancy Services</li>
                         </ul> 
                        </div>
                    </div>
                    <div class="tab-pane " id="audit" role="tabpanel" aria-labelledby="audit-tab">
                        <div class="tab_contnets_inner_box">
                            <h2>Audit & Assurance:</h2>
                            <p>We NPRP & Co; have extensive experience in providing Audit & Assurance services under
                                various statutes across various industries. We mainly practice in the following areas:
                            </p>
                            <ul>
                                <li>Statutory Audit under Companies Law</li>
                                <li>Statutory & Concurrent Audit of Banks</li>
                                <li>Audit of Co-Operative Societies & NGO</li>
                                <li>Tax Audit under Income Tax Act</li>
                                <li>GST Audit under GST Law</li>
                                <li>Internal & Management Audit</li>
                                <li>Special Audit under various Statutes</li>
                                <li>Agreed Upon Procedures (AUP) as per SRS 4400</li>
                                <li>Stock Audit</li>
                            </ul>
                        </div>
                    </div>
                  
                    <div class="tab-pane" id="direct_tax" role="tabpanel" aria-labelledby="direct_tax-tab">
                       <div class="tab_contnets_inner_box">
                       <h3>Direct Tax:</h3>
                        <p>We NPRP & Co; are providing services in the field of tax compliance, tax audit, representation and litigation matters and providing tax advisory.</p>
                        <ul>
                            <li>Tax planning for individuals, firms and corporate bodies</li>
                            <li>Preparation and filing Tax Returns (ITR)</li>
                            <li>Tax Audit under Income Tax Act</li>
                            <li>TDS & TCS Compliances</li>
                            <li>International Taxation</li>
                            <li>Representation and Litigation</li>
                            <li>Taxation relating certification</li>
                            <li>Tax Consultancy</li> 
                        </ul>
                       </div>
                    </div>
                    <div class="tab-pane" id="settings" role="tabpanel" aria-labelledby="settings-tab">

                    </div>
                </div>
            </div>

            <div class="col-md-5">
              <div class="service_inner_wrap_layout">
                <div class="right_bg">

                </div>
                <div class="service_right_quick_access">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                            <a class="nav-link active" id="goods-tab" data-toggle="tab" href="#goods" role="tab"
                                aria-controls="goods" aria-selected="false">Goods and Services Tax</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="audit-tab" data-toggle="tab" href="#audit" role="tab"
                                aria-controls="audit" aria-selected="true">Audit & Assurance</a>
                        </li>
                       
                        <li class="nav-item">
                            <a class="nav-link" id="direct_tax-tab" data-toggle="tab" href="#direct_tax" role="tab"
                                aria-controls="direct_tax" aria-selected="false">Direct Tax:</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="settings-tab" data-toggle="tab" href="#" role="tab"
                                aria-controls="settings" aria-selected="false">Business Setup & Registration</a>
                        </li>
                    </ul>
                </div>
              </div>      
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php')?>