<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>NPRP & Co.</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendors/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/custom.css">
  <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/media.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/flaticon.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/docs.theme.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/assets/owl.theme.default.min.css">
  <script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
</head>

<body>
  <section class="header_s">
    <div class="top_header">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="mt_topbar_left clearfix">
              <ul class="list_top_header">
                <li class="active_link"><i class="fas fa-home"></i><a href="index.php"> Home</a></li>
                <li><i class="far fa-envelope"></i><a href="mailto:nprp@gmail.com">nprp@gmail.com</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="mt_topbar_right">
              <ul>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-linkedin"></i></a></li>
                <li><a href="#"><i class="ti-youtube"></i></a></li>
                <li class="right_border left_border"><a href="tel:+91-9891207090" class="page_one">+91-9891207090</a>
                </li>
                <li><a href="contact-us.php" class="page_one">Request Call Back</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main_header">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="logo_sec">
              <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></a>
            </div>
          </div>
          <div class="col-md-9">
            <div class="main_header_menu">
              <ul>
                <li><a href="<?php echo base_url();?>about">About Us</a></li>
                <li><a href="<?php echo base_url();?>services">Our Services</a>
                  <div class="drop_down_wrpa">
                    <div class="dropdown_inner">
                      <div class="col_drop_down">
                        <ul>
<?php 
if(count($service)>0){
  foreach($service as $serv){
?>
                          <li><a href="<?php echo base_url();?>service/service-details/<?php echo $serv['id']; ?>"><?php echo $serv['service_name']; ?></a></li>
<?php }}?>
                          <!-- <li><a href="good-service-tax.php">Good and Services Tax(GST)</a></li>
                          <li><a href="direct-tax.php">Direct Tax</a></li>
                          <li><a href="#">Accounting and Business Process Outsourcing</a></li>
                          <li><a href="#">Business Setup & Registration</a></li> -->
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
                <li><a href="<?php echo base_url();?>team">Our Team</a></li>
                <li><a href="#">Resources</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
                <li><a href="<?php echo base_url();?>contact"><button class="btn btn_danger long_btn20">Book an Appointment</button></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>




  <section class="mobile_menu">
    <div class="mobile_menu_logo">
      <a href="index.php"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></a>
    </div>
    <div class="mobo_button">
      <span class="one"></span>
      <span class="two"></span>
      <span class="three"></span>
    </div>
    <div class="mobo__menu_link" id="overlay">
      <ul>
        <li><a href="<?php echo base_url();?>about">About Us</a></li>
        <li><a href="#">Our Services</a>
          <div class="drop_down_wrpa">
            <div class="dropdown_inner">
              <div class="col_drop_down">
                <ul>
                  <li><a href="audit-assurance.php">Audit & Assurance</a></li>
                  <li><a href="good-service-tax.php">Good and Services Tax(GST)</a></li>
                  <li><a href="direct-tax.php">Direct Tax</a></li>
                  <li><a href="#">Accounting and Business Process Outsourcing</a></li>
                  <li><a href="#">Business Setup & Registration</a></li>
                </ul>
              </div>
              <div class="col_drop_down">
                <ul>
                  <li><a href="#">Corporate Secretarial Services</a></li>
                  <li><a href="#">Certification</a></li>
                  <li><a href="#">Financial Services</a></li>
                  <li><a href="#">Valuation</a></li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        <li><a href="<?php echo base_url();?>team">Our Team</a></li>
        <li><a href="#">Resources</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="<?php echo base_url();?>contact">Contact Us</a></li>
        <!-- <li><button class="btn btn_danger long_btn20">Book an Appointment</button></li> -->
      </ul>
    </div>
  </section>