    
   <section class="main__footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="foot_contact_wrap">
                        <div class="footer_logo">
                            <h6>NPRP & Co.</h6>
                        </div>
                        <div class="contact__options">
                            <ul>
                                <li><i class="fas fa-map-marker-alt"></i>55 Ground Floor, Bapu Park,Kotla Mubarkh
                                    Pur,New-Delhi-110003</li>
                                <li><i class="fas fa-phone"></i>+91-9971014147</li>
                                <li><i class="fas fa-envelope"></i><a
                                        href="mailto:info@nprp.com">info@nprp.com</a></li>
                            </ul>
                        </div>
                        <div class="foot_social">
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer__links">
                        <h6 class="green_foot">Quick Links</h6>
                        <ul class="link_list">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">People Strength</a></li>
                            <li><a href="#">Industries</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer__links">
                        <h6 class="green_foot">Our Services</h6>
                        <ul class="link_list">
                            <li><a href="#">Audit & Assurance</a></li>
                            <li><a href="#">Direct Taxes</a></li>
                            <li><a href="#">Indirect Taxes</a></li>
                            <li><a href="#">Business Advisory</a></li>
                            <li><a href="#">IT Risk Advisory</a></li>
                            <li><a href="#">Accounting & Business Process</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer__links">
                        <h6 class="green_foot">Need Help?</h6>
                        <ul class="link_list">
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Policy</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Get a Quote</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Career</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="foot__bootom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <p class="copyright_text">© Copyrights 2019 nprp & co. All rights reserved</p>
                </div>
                <div class="col-md-4">
                    <div class="team_sec">
                        <p>Design by | <a href="#">Team WKD</a> </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


  <script src="<?php echo base_url();?>assets/js/main.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
  <script>
    baguetteBox.run('.tz-gallery');
</script>
  
</body>
</html>