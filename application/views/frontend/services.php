<?php include('inc/header.php')?>
<section class="bredcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h2>Our Services</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services_inner_sec">
     <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="service_offer_box">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/service1.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Audit & Assurance</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_offer_box">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/service6.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Direct Taxes</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_offer_box">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/service5.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Indirect Taxes</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_offer_box mts_top">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/GST.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Good & Service Tax ( GST)</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_offer_box mts_top">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/setups.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Business Setup & Registration</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_offer_box mts_top">
                    <div class="service_Offer_box_inner">
                        <img src="assets/img/CORPORATE.jpg" alt="">
                    </div>
                    <div class="service_offer_box_content_wrap">
                        <div class="service_content_box_desc">
                            <h4>Corporate Secretarial Services</h4>
                            <p>Extensive experience in providing assurance services under various statutes across various
                                industries
                                for the past several decades.</p>
                            <a href="#" class="moredn_btn">
                                Learn More
                                <div class="strips"></div>
                                <i class="ti-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
</section>


<?php include('inc/footer.php')?>